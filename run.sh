#!/usr/bin/env bash
nasm -f elf64 -g -F dwarf -o attack.o attack.asm
ld --fatal-warnings -o attack attack.o
strace ./attack $1