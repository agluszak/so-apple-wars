#include<byteswap.h>
#include<stdio.h>

int main(int argc, char** argv) {
    if (argc != 2) return 1;
    FILE* ptr = fopen(argv[1], "rb");
    unsigned int num;
    while(fread(&num, sizeof(int), 1, ptr) != 0) {
        printf("%d\n", bswap_32(num));
    }
    fclose(ptr);
    return 0;
}
