section .data
    bufLen equ 4096
    magic equ 68020
    numbers:
        dd 6
        dd 8
        dd 0
        dd 2
        dd 0

section .bss
    buf resb bufLen

section .text
global _start

; r12d - offset w buforze
; r13 - czy znaleziono liczbę z odpowiedniego przedziału
; r14d - suma
; r15 - ile znaleziono kolejnych liczb

%define offset r12d
%define foundFromRange r13
%define sum r14d
%define found r15

_start:
    pop rcx ; czytamy argc
    cmp rcx, 2
    jne errorExit ; program powinien zostać wywołany z dokładnie jednym argumentem

    mov rax, 2 ; open(nazwa pliku, tryb, uprawnienia)
    pop rdi ; pomijamy jedną rzecz ze stosu (argv[0] - nazwę programu)
    pop rdi ; nazwa pliku - z linii poleceń
    xor rsi, rsi ; tryb read-only
    mov rdx, 777 ; uprawnienia
    syscall ; open
    test rax, rax
    js errorExit ; przerywamy jeśli otwieranie się nie udało

    mov rdi, rax ; zapamiętajmy deskryptor pliku
    xor offset, offset ; niczego nie przejrzeliśmy
    xor foundFromRange, foundFromRange ; ustawiamy flagi
    xor sum, sum ; zerujemy sumę
    xor found, found ; nie znaleźliśmy jeszcze żadnej liczby

loadChunk:
    xor rax, rax ; read(deskryptor, bufor, ilość bajtów)
    mov rsi, buf ; bufor
    mov rdx, bufLen ; ilość bajtów
    syscall ; read
    test rax, rax
    js errorExit ; błąd
    ; teraz policzymy, ile licz 32-bitowych jest we wczytanym bloku
    ; jeśli wynik dzielenia jest niepodzielny przez 4, to znaczy, że plik jest niepoprawny
    mov rdx, rax
    and rdx, 3 ; sprawdzamy podzielność przez 4

    test rdx, rdx
    jnz errorExit ; liczba bajtów niepodzielna przez 4

    test rax, rax
    jz endOfFile ; koniec pliku

    shr rax, 2 ; pamiętamy ile liczb wczytaliśmy (a nie bajtów)
    mov rcx, rax
    xor offset, offset

examineNumber:
    mov ebx, DWORD [buf+offset] ; bierzemy liczbę ze stosu
    bswap ebx ; odwracamy bajty
    cmp ebx, magic
    je errorExit ; tej liczby miało nie być
    jl notFoundFromRange
    or foundFromRange, 1 ; zaznaczamy że znaleźliśmy liczbę z przedziału

notFoundFromRange:
    add sum, ebx ; zwiększamy sumę
    cmp found, 5
    je done ; zatrzymujemy się po znalezieniu wszystkich z sekwencji
    inc found
    cmp ebx, DWORD [numbers+found*4-4] ; sprawdzamy czy jest liczba z sekwencji
    je done
    xor found, found  ; nie znaleziono odpowiedniej liczby z sekwencji
    cmp ebx, DWORD [numbers] ; sprawdzamy czy to nie jest początek nowej sekwencji
    jne done
    inc found

done:
    add offset, 4
    loop examineNumber ; czytamy kolejną liczbę z bloku
    jmp loadChunk ; po przejrzeniu całego bloku wczytujemy kolejny

endOfFile: ; na koniec sprawdzamy czy spełnione są wszystkie warunki
    cmp sum, magic
    jne errorExit
    cmp foundFromRange, 1
    jne errorExit
    cmp found, 5
    jne errorExit
    mov rax, 60
    xor rdi, rdi
    syscall

errorExit:
    mov rax, 60
    mov rdi, 1
    syscall
